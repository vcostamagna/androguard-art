import sys
import zlib
import binascii, struct
from struct import pack, unpack, calcsize
from elfesteem import *
import logging

log = logging.getLogger("elfparse")
console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter("%(levelname)-5s: %(message)s"))
log.addHandler(console_handler)
log.setLevel(logging.WARN)

ART_FILE_MAGIC_39 = 'oat\n039\x00'



INSTRUCTION_SET = [
    (0x1, 'kNone'),
    (0x2, 'kArm'),
    (0x3, 'kArm64'),
]

class _Bytecode(object):
    def __init__(self, buff):
        try:
            import psyco
            psyco.full()
        except ImportError:
            pass

        self.__buff = buff
        self.__idx = 0

    def read(self, size):
        #if isinstance(size, SV):
        #    size = size.value

        buff = self.__buff[ self.__idx : self.__idx + size ]
        self.__idx += size

        return buff

class OATformat(_Bytecode):
    def __init__(self, buff):
        super(OATformat, self).__init__(buff)

        self._preload()
        self._load(buff)

    def _preload(self):
        pass
    def _load(self,buff):
        self.__header = HeaderItem(self)
        self.__header.show()

class HeaderItem(object):
    """
    This class can parse an header of OAT file
    """
    def __init__(self,buff):
        #print str(buff[:4])
        #self._header = pack("=Q", buff[:8])
        self._magic = unpack("=l", buff.read(4))[0]
        self._version = unpack("=l", buff.read(4))[0]
        self._checksum = unpack("=I", buff.read(4))[0]
        self._iset = unpack("=I", buff.read(4))[0]
        self._iset_features = unpack("=I", buff.read(4))[0]
        self._dex_file_count = unpack("=I", buff.read(4))[0]
        self._exec_offset = unpack("=I", buff.read(4))[0]
        self._int_to_int_bridge_off = unpack("=I", buff.read(4))[0]
        self._int_to_obj_code_bridge_off = unpack("=I", buff.read(4))[0]
        self._jni_dlsym_lookup_off = unpack("=I", buff.read(4))[0]
        self._portable_imt_conflict_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._portable_resolution_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._portable_to_interpreter_bridge_offset_ = unpack("=I", buff.read(4))[0]
        self._uint32_t quick_generic_jni_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._uint32_t quick_imt_conflict_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._uint32_t quick_resolution_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._uint32_t quick_to_interpreter_bridge_offset_ = unpack("=I", buff.read(4))[0]


    def parse_content(self):
        pass
    def show(self):
        print pack("=l",self._magic)
        print pack("=l",self._version)
        print binascii.hexlify(pack("=I", self._dex_file_count))



if __name__ == "__main__":
    import rlcompleter,readline,pdb
    E = elf_init.ELF(open(
        "/home/sid/Dropbox/Dottorato/ProgettoPhd/art/lollipop/"+\
        "testing/data@local@tmp@HelloWorld.zip@classes.dex").read())
    E.parse_content()
    print repr(E)
    print repr(E.sh)
    rodata = E.sh.__getitem__(4) #get .rodata section
    print repr(rodata)
    roff = rodata.sh.__getitem__("offset")
    print roff

    oatf = OATformat(E.content[roff:])
    #print str(E.content[size:size+8])


