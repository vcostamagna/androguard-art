import binascii
import sys
import dvm
from struct import pack, unpack, calcsize
from elfesteem import *
import logging
from androguard.core import bytecode
import os
import mmap

from androguard.util import read
'''
from r2.r_core import *
from r2.r_bin import *

log = logging.getLogger("elfparse")
console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter("%(levelname)-5s: %(message)s"))
log.addHandler(console_handler)
log.setLevel(logging.WARN)
'''

ART_FILE_MAGIC_39 = 'oat\n039\x00'

INSTRUCTION_SET = [
    (0x1, 'kNone'),
    (0x2, 'kArm'),
    (0x3, 'kArm64'),
]

class OatClass(object):

    def __init__(self, buff):
        self._status_ = unpack("=i", buff.read(4))[0]

class OatDexFileHeader(object):
    """
    const std::string dex_file_location_size_;
    const uint32_t dex_file_location_checksum_;
    const byte* const dex_file_pointer_;
    const uint32_t* const oat_class_offsets_pointer_;
    """
    _oat_class_offset = []
    def __init__(self,buff):
        #grandezza della stringa dex_file_location che segue
        #self._dex_file_location_size_off = buff.tell()
        self._dex_file_location_size = unpack("<I", buff.read(4))[0]
        '''
        stringa dex_file_location grossa dex_file_location_size
        '''
        #self._dex_file_location_data_off = buff.tell()
        self._dex_file_location_data = buff.read(self._dex_file_location_size)
        #print self._dex_file_location_data
        '''
        checksum 
        '''
        #self._dex_file_checksum_off = buff.tell()
        self._dex_file_checksum = unpack("<I", buff.read(4))[0]
        '''
        offset del DexFile rispetto a begin_ (oatdata)
        '''
        #self._dex_file_offset_off = buff.tell()
        self._dex_file_offset = unpack("=I", buff.read(4))[0]

        self._classes_offsets = None

    def get_dex_file_location_data(self):
        return self._dex_file_location_data

    def get_dex_file_checksum(self):
        return self._dex_file_checksum

    def _set_oatclass_offset(self, buff):
        '''
        offset di oatclass rispetto a begin_
        '''
        self._oat_class_offset.append(unpack("=I", buff.read(4))[0])

    def set_dex_file_location_data(self, buff, newpath):
        buff.seek(self._dex_file_location_data_off)
        buff.write(newpath)
        pass

    def set_dex_file_checksum(self, buff, newck):
        buff.seek(self._dex_file_checksum_off)
        buff.write(pack('<I',newck))
        pass

    def show(self):
        bytecode._PrintDefault("dex_file_location_data=%s\n" % (self._dex_file_location_data))
        bytecode._PrintDefault("dex_file_checksum=0x%x\n" % (self._dex_file_checksum))
        bytecode._PrintDefault("dex_file_offset=0x%x\n" % (self._dex_file_offset))


class StoreKeyValue():
    def __init__(self,buff,size):
        self.parse(buff,size)
    def parse(self, buff,size):
        self._key_store = buff.read(size)

class OATformat(bytecode._Bytecode):
    def __init__(self, filename, buff):
        super(OATformat, self).__init__(buff)
        #dvm.DalvikVMFormat(buff)
        self.__oatmanager = OATManager()
        
        self.mem_buff = buff
        
        #self.mem_buff = self.memory_map(filename)
        #self.mem_buff.seek(0x1000, os.SEEK_SET) # .oatdata
        self.__oatdexfile_list = []
        self.__dexfile_list = []
        self._preload()
        self._load()

    def end(self):
        self.mem_buff.close()
        
    def memory_map(self,filename, access=mmap.ACCESS_WRITE):
        size = os.path.getsize(filename)
        fd = os.open(filename, os.O_RDWR)
        return mmap.mmap(fd.fileno(), size, access=access)

    def set_oatdexfile_path(self, newpath):
        tmp = self.__oatdexfile_list[0]
        tmp.set_dex_file_location_data(self.mem_buff, newpath)

    def set_oatdexfile_checksum(self, newck):
        tmp = self.__oatdexfile_list[0]
        tmp.set_dex_file_checksum(self.mem_buff, newck)

    def get_oatdexfile_path(self):
        tmp = self.__oatdexfile_list[0]
        return tmp.get_dex_file_location_data()

    def get_oatdexfile_checksum(self):
        tmp = self.__oatdexfile_list[0]
        return tmp.get_dex_file_checksum()

    def _preload(self):
        pass
    def _load(self):
        self.__header = HeaderItem(self)
        size = sys.getsizeof(self.__header)
        print "founded dex files:\n" + str(self.__header._dex_file_count)
        print 'header size %d \n' % (size)
        tmp = OatDexFileHeader(self)
        self.__oatdexfile_list.append(tmp)
        tmp.show()
        #return
        for adex in self.__oatdexfile_list: #range(self.__header._dex_file_count):
            #tmp = OatDexFile(self.mem_buff.seek( 0x1000 + adex._dex_file_offset))
            #tmp.show()
            #sys.exit(-1)
            #off = 0x1000 + size + tmp._dex_file_offset
            #print 'vado a: %d' % (off)
            #self.mem_buff.seek(off)
            dexoff = 0x1000 + adex._dex_file_offset
            #print 'DEX OFF: 0x%x \n ' % ( dexoff)
            #self.mem_buff.seek( dexoff, os.SEEK_SET )
            #print 'DEX: %s \n' % (self.mem_buff[dexoff:dexoff+8])
            #print 'sono ad off = 0x%x \n ' % (self.mem_buff.tell())
            #diocan = self.mem_buff[dexoff:]
            self.set_idx(dexoff)
            print 'OATFormat idx vale %x' % (self.get_idx() )
            print 'buff lungo : %d ' % (self.length_buff())
            #diocan = self.read_off(dexoff, self.length_buff() - self.get_idx() )
            dextmp = dvm.DalvikVMFormat(open('/home/sid/ownCloud/documents/DarkSide/mala/android-melter/com.wsandroid.suite.apk_FILES/classes.dex','r').read())
            dexheader = dextmp.get_header_item()
            nclassdef = dexheader.class_defs_size
            print "founded classes: " + str(nclassdef) + "\n"
            for i in range(nclassdef):
                tmp._set_oatclass_offset(self)
            dexheader.show()
            sys.exit(-1)
            #self.__oatdexfile_list.append(tmp)
            #self.__dexfile_list.append(dextmp)
        self.__header.show()


class OatClassHeader(object):
    def __init__(self, buff):
        self._status = unpack('=H', buff.read(2))[0]
        self._type = unpack('=H', buff.read(2))[0]
        self._bitmap_size = unpack('=I', buff.read(4))[0]
        self._bitmap = readOatClassHeaderBitmap(buff, self._bitmap_size) #only when type = 1
        self._methods_offsets = None



class OATManager(object):
    def __init__(self):
        pass

class HeaderItem(object):
    """
    This class can parse an OAT file header
    """
    def __init__(self, buff):
        print 'HeaderItem %s ' %(buff)
        buff.set_idx(0x1000)
        #print 'DEBUG: %s' % (buff.read(4))
        #self._magic_off = buff.tell()
        self._magic = unpack("=l", buff.read(4))[0]
        #self._version_off = buff.tell()
        self._version = unpack("=l", buff.read(4))[0]
        #self._checksum_off = buff.tell()
        self._checksum = unpack("=I", buff.read(4))[0]
        self._iset = unpack("=I", buff.read(4))[0]
        self._iset_features = unpack("=I", buff.read(4))[0]
        self._dex_file_count = unpack("<I", buff.read(4))[0]
        self._exec_offset = unpack("=I", buff.read(4))[0]
        self._int_to_int_bridge_off = unpack("=I", buff.read(4))[0]
        self._int_to_obj_code_bridge_off = unpack("=I", buff.read(4))[0]
        self._jni_dlsym_lookup_off = unpack("=I", buff.read(4))[0]
        self._portable_imt_conflict_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._portable_resolution_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._portable_to_interpreter_bridge_offset_ = unpack("=I", buff.read(4))[0]
        self._quick_generic_jni_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._quick_imt_conflict_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._quick_resolution_trampoline_offset_ = unpack("=I", buff.read(4))[0]
        self._quick_to_interpreter_bridge_offset_ = unpack("=I", buff.read(4))[0]
        self._image_patch_delta_ =  unpack("=I", buff.read(4))[0]
        self._image_file_location_oat_checksum_ =  unpack("=I", buff.read(4))[0]
        self._image_file_location_oat_data_begin_ =  unpack("=I", buff.read(4))[0]
        self._key_value_store_size_ = unpack("=I", buff.read(4))[0]
        self._key_value_store_ = StoreKeyValue(buff,int(self._key_value_store_size_))
        #qui finisce header oat
        self.show()

    def set_magic(self, buff, oatm):
        buff.seek( self._magic_off )

    def show(self):
        bytecode._PrintDefault("magic=%s, version%s\n" % (pack("<l", self._magic), pack("=l",self._version) ))
        bytecode._PrintDefault("checksum=%x, iset=%d\n" % (self._checksum, int(self._iset) ))
        bytecode._PrintDefault("iset_features=%x, dexfilecount=%d\n" % (self._key_value_store_size_, int(self._dex_file_count) ))
        bytecode._PrintDefault("exec_offset=%x, introintB=%x\n" % (self._exec_offset, self._int_to_int_bridge_off))
        bytecode._PrintDefault("oatchecksum=%x, oatdata_begin=%x\n" % (self._image_file_location_oat_checksum_, self._image_file_location_oat_data_begin_))

if __name__ == "__main__":
    import rlcompleter,readline,pdb
    if len(sys.argv) > 2:
        oat_file_original   = sys.argv[1]
        oat_file_patched = sys.argv[2]
    else:
        sys.exit()
    ''''    
    io = RIO()
    desc = io.open(oat_file,0, 0)
    b = RBin()
    b.iobind(io)
    b.load(oat_file, 0 , 0, 0, desc.fd, False)

    E = elf_init.ELF(open(oat_file).read())
    E.parse_content()
    print repr(E)
    print repr(E.sh)
    rodata = E.sh.__getitem__(4) #get .rodata section
    print repr(rodata)
    roff = rodata.sh.__getitem__("offset")
    begin_ = roff
    print roff
    '''

    buff1 = open(oat_file_original, 'r').read()
    buff2 = open(oat_file_patched, 'r').read()
    oatf1 = OATformat(oat_file_original, buff1)
    oatf2 = OATformat(oat_file_original, buff2)

    #oatf1 = OATformat(oat_file_original)
    #oatf2 = OATformat(oat_file_patched)
    path1 = oatf1.get_oatdexfile_path()
    ck1 = oatf1.get_oatdexfile_checksum()
    #oatf2.set_oatdexfile_path(path1)
    #oatf2.set_oatdexfile_checksum(ck1)
    oatf1.end()
    oatf2.end()

